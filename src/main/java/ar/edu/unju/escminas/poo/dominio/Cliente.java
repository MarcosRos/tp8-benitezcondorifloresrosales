package ar.edu.unju.escminas.poo.dominio;

public class Cliente {
    private long dni;
    private String nombre;
    private String apellido;
    private String tipoCliente;
    private Plan planCliente;

    public Cliente() {
        // TODO Auto-generated constructor stub
    }

    public Cliente(long dni, String nombre, String apellido, String tipoCliente, Plan planCliente) {
        super();
        this.dni = dni;
        this.nombre = nombre;
        this.apellido = apellido;
        this.tipoCliente = tipoCliente;
        this.planCliente = planCliente;
    }

    public long getDni() {
        return dni;
    }

    public void setDni(long dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public Plan getPlanCliente() {
        return planCliente;
    }

    public void setPlanCliente(Plan planCliente) {
        this.planCliente = planCliente;
    }

    @Override
    public String toString() {
        return "Cliente [dni=" + dni + ", nombre=" + nombre + ", apellido=" + apellido + ", tipoCliente=" + tipoCliente
                + ", planCliente=" + planCliente + "]";
    }

}