package ar.edu.unju.escminas.poo.dominio;

import java.time.LocalDate;

import ar.edu.unju.escminas.poo.tablas.TablaClientes;
import ar.edu.unju.escminas.poo.tablas.TablaCuotas;
import ar.edu.unju.escminas.poo.tablas.TablaElectrodomesticos;

public class Empleado {

	private String username;
	private String password;

	public Empleado() {
		// TODO Auto-generated constructor stub
	}

	public Empleado(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Empleado [username=" + username + ", password=" + password + "]";
	}

	public void crearCliente(Cliente unCliente) {
		TablaClientes.clientes.add(unCliente);
	}

	public void crearElectrodomestico(Electrodomestico unElectrodomestico) {
		TablaElectrodomesticos.electrodomesticos.add(unElectrodomestico);
	}

	public void aniadirStock(int pos, int stockNuevo) {
		int stockTotal = TablaElectrodomesticos.electrodomesticos.get(pos).getStock() + stockNuevo;
		TablaElectrodomesticos.electrodomesticos.get(pos).setStock(stockTotal);
	}

	public void cuotasImpagas(Cliente unCliente) {
		if (unCliente.getPlanCliente() == null) {
			System.out.println("El cliente no ha comprado ningun articulo. No hay cuotas que mostrar");
		} else {
			for (int j = 0; j < unCliente.getPlanCliente().getCuotas().size(); j++) {
				if (unCliente.getPlanCliente().getCuotas().get(j).getEstado() == false) {
					System.out.println(unCliente.getPlanCliente().getCuotas().get(j));
				}
			}
		}
	}

	public void cuotasPagadas(Cliente unCliente) {
		if (unCliente.getPlanCliente() == null) {
			System.out.println("El cliente no ha comprado ningun articulo. No hay cuotas que mostrar");
		} else {
			for (int j = 0; j < unCliente.getPlanCliente().getCuotas().size(); j++) {
				if (unCliente.getPlanCliente().getCuotas().get(j).getEstado() == true) {
					System.out.println(unCliente.getPlanCliente().getCuotas().get(j));
				}
			}
		}
	}

	public void agregarPlanCliente(int pos, Plan unPlan) {
		TablaClientes.clientes.get(pos).setPlanCliente(unPlan);
	}

	public void planCliente(Cliente unCliente) {
		System.out.println(unCliente.getPlanCliente());
	}

	public void montoAPagar(Cliente unCliente) {
		System.out.println(unCliente.getPlanCliente().getMontoPorPagar());
	}

	public void dineroPorCobrar() {
		double total = 0;
		if (TablaCuotas.cuotas.isEmpty()) {
			System.out.println("No hay cuotas generadas en el sistema, ");
			System.out.println("El dinero que la empresa tiene por cobrar aun es de 0 pesos");
		} else {
			for (int i = 0; i < TablaClientes.clientes.size(); i++) {
				if (TablaClientes.clientes.get(i).getPlanCliente()!=null) {
				total = total + TablaClientes.clientes.get(i).getPlanCliente().getMontoPorPagar();
				}
			}
			System.out.println("El dinero que la empresa tiene por cobrar aun es de: " + total + " pesos");
		}
	}

	public void clienteMenosDeudor() {
		int deudoresIguales = 0, tamanio = TablaClientes.clientes.size(), pos = 0;
		double deuda=0;
		boolean band = false;

		if (tamanio == 1) {
			if (TablaClientes.clientes.get(0).getPlanCliente() == null) {
				System.out.println("No existen clientes con plan de pagos");
			} else {
				System.out.println(TablaClientes.clientes.get(0));
			}
		} else {
			do {
				if (TablaClientes.clientes.get(pos).getPlanCliente() != null) {
					deuda = TablaClientes.clientes.get(0).getPlanCliente().getMontoPorPagar();
					band = true;
				}
				pos++;

			} while (band == false && pos <= tamanio);

			if (band == true) {
				for (int i = 0; i < TablaClientes.clientes.size(); i++) {

					if (TablaClientes.clientes.get(i).getPlanCliente() != null) {
						if (TablaClientes.clientes.get(i).getPlanCliente().getMontoPorPagar() != 0) {
							if (TablaClientes.clientes.get(i).getPlanCliente().getMontoPorPagar() == deuda) {
								deudoresIguales++;
							} else if (TablaClientes.clientes.get(i).getPlanCliente().getMontoPorPagar() < deuda) {
								deudoresIguales = 1;
								deuda = TablaClientes.clientes.get(i).getPlanCliente().getMontoPorPagar();
							}
						}
					}
				}
				if (deudoresIguales >= 2) {
					System.out.println("Hay " + deudoresIguales + " clientes con la misma deuda restante");
				}
				while (deudoresIguales != 0) {
					for (int i = 0; i < TablaClientes.clientes.size(); i++) {
						if (TablaClientes.clientes.get(i).getPlanCliente().getMontoPorPagar() == deuda) {
							System.out.println(TablaClientes.clientes.get(i));
							deudoresIguales--;
						}
					}
				}
			}else if (pos>tamanio) {
				System.out.println("No existen clientes con plan de pagos");
			}
		}
	}

	public void registrarPago(Cliente unCliente) {
		boolean band = false;
		Cuota unaCuota = null;
		if (unCliente.getPlanCliente() == null) {
			System.out.println("No existen cuotas para este cliente");
		} else {

			for (int i = 0; i < unCliente.getPlanCliente().getCuotas().size() && band == false; i++) {
				if (unCliente.getPlanCliente().getCuotas().get(i).getEstado() == false) {
					unCliente.getPlanCliente().getCuotas().get(i).setEstado(true);
					band = true;
					unCliente.getPlanCliente().setMontoPorPagar(unCliente.getPlanCliente().getMontoPorPagar()
							- unCliente.getPlanCliente().getCuotas().get(i).getMonto());
					unaCuota = unCliente.getPlanCliente().getCuotas().get(i);
					unaCuota.setFechaPaga(LocalDate.now());
				}
			}
			if (band == true)
				System.out.println(unaCuota);
			else
				System.out.println("El cliente no debe ninguna otra cuota");

		}
	}

}
