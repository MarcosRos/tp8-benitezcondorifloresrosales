package ar.edu.unju.escminas.poo.dominio;

import java.time.LocalDate;

public class Cuota {
    private double monto;
    private LocalDate fechaVencimiento;
    private Boolean estado;
    private LocalDate fechaPaga;

    public Cuota() {
        // TODO Auto-generated constructor stub
    }

    public Cuota(double monto, LocalDate fechaVencimiento, Boolean estado, LocalDate fechaPaga) {
        super();
        this.monto = monto;
        this.fechaVencimiento = fechaVencimiento;
        this.estado = estado;
        this.fechaPaga = fechaPaga;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public LocalDate getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public LocalDate getFechaPaga() {
        return fechaPaga;
    }

    public void setFechaPaga(LocalDate fechaPaga) {
        this.fechaPaga = fechaPaga;
    }

    @Override
    public String toString() {
    	String estadoCuota,fecha;
    	if (estado==false){
    		estadoCuota="No pagada";
    	}
    	else
    	{
    		estadoCuota="Pagada";
    	}
    	
    	
    	if (fechaPaga==null)
    	{
    		fecha="----/--/--";
    	}
    	else {
    		fecha=fechaPaga.toString();
    	}
    	
    	
        return "Cuota [monto=" + monto + ", fechaVencimiento=" + fechaVencimiento + ", estado=" + estadoCuota
                + ", fechaPaga=" + fecha + "]";
    }

}
