package ar.edu.unju.escminas.poo.principal;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

import ar.edu.unju.escminas.poo.dominio.Cliente;
import ar.edu.unju.escminas.poo.dominio.Cuota;
import ar.edu.unju.escminas.poo.dominio.Electrodomestico;
import ar.edu.unju.escminas.poo.dominio.Empleado;
import ar.edu.unju.escminas.poo.dominio.Plan;
import ar.edu.unju.escminas.poo.tablas.TablaClientes;
import ar.edu.unju.escminas.poo.tablas.TablaCuotas;
import ar.edu.unju.escminas.poo.tablas.TablaElectrodomesticos;

public class Principal {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Creamos un unico eempleado
		Empleado unEmpleado = new Empleado("Pepe", "asd123");

		Scanner scan = new Scanner(System.in);
		String contrasenia, usuario;
		Boolean login = false;
		do {
			System.out.println("Ingrese el usuario");
			usuario = scan.next();
			if (usuario.equals(unEmpleado.getUsername())) {
				login = true;
			} else {
				System.out.println("No existe un usuario con ese nombre");
			}
		} while (login == false);
		login = false;

		do {
			System.out.println("Ingrese la contrasenia");
			contrasenia = scan.next();
			if (contrasenia.equals(unEmpleado.getPassword())) {
				System.out.println("Contrasenia correcta!");
				login = true;
			}
		} while (login == false);

		int opcion = 0;
		int contadorLavarropa = 0, contadorHeladera = 0, contadorCocina = 0, contadorTermotanqueAGas = 0,
				contadorTermotanqueElectrico = 0, contadorLavavajillas = 0, contadorVentiladores = 0;
		boolean bandera = false;
		do {
			do {
				bandera = true;
				System.out.println("Menu Empleado");
				System.out.println("1) Agregar Electrodomestico");
				System.out.println("2) Crear y Agregar Plan");
				System.out.println("3) Ver cuotas impagas de un Cliente");
				System.out.println("4) Registar pago de cuota");
				System.out.println("5) Ver la cantidad de dinero por cobrar");
				System.out.println("6) Mostar al cliente que menos debe");
				System.out.println("7) Agregar Cliente");
				System.out.println("8) Salir");
				try {
					opcion = scan.nextInt();
				} catch (InputMismatchException ime) {
					System.out.println("Formato Incorrecto");
					bandera = false;
					scan.next();
				}
			} while (bandera == false);

			switch (opcion) {
			case 1:
				int opcionElectrodomestico = 0, codigoElectrodomestico = 0, posicionElectrodomestico = 0,
						cantidadStockElectrico = 0;
				String marcaElectronico, tipoElectronico, modeloElectronico = null;
				double precioElectronico = 0;
				do {
					bandera = true;
					System.out.println("1)Agregar Stock a un producto ya existente");
					System.out.println("2)Agregar un nuevo producto");
					try {
						opcionElectrodomestico = scan.nextInt();
					} catch (InputMismatchException ime) {
						bandera = false;
						System.out.println("Formato Incorrecto");
						scan.next();
					}
				} while (bandera == false);

				if (opcionElectrodomestico == 1) {
					if (TablaElectrodomesticos.electrodomesticos.isEmpty()) {
						System.out.println("No hay ningun electrodomestico");
					} else {
						do {
							bandera = true;
							System.out.println("Ingrese el codigo del producto a agregar Stock");
							try {
								codigoElectrodomestico = scan.nextInt();
							} catch (InputMismatchException ime) {
								System.out.println("Formato Incorrecto");
								bandera = false;
								scan.next();
							}
						} while (bandera == false);

						bandera = false;
						for (int i = 0; i < TablaElectrodomesticos.electrodomesticos.size(); i++) {
							if (codigoElectrodomestico == TablaElectrodomesticos.electrodomesticos.get(i).getCodigo()) {
								bandera = true;
								posicionElectrodomestico = i;
							}
						}

						if (bandera == false) {
							System.out.println("No existe ningun electrodomestico con ese codigo");
						} else {
							do {
								bandera = true;
								System.out.println("Ingrese la cantidad de Stock a agregar al Producto");
								try {
									cantidadStockElectrico = scan.nextInt();
								} catch (InputMismatchException ime) {
									System.out.println("Formato Incorrecto");
									bandera = false;
									scan.next();
								}
							} while (bandera == false);

							unEmpleado.aniadirStock(posicionElectrodomestico, cantidadStockElectrico);
						}
					}
				} else if (opcionElectrodomestico == 2) {
					Boolean banderaAgregarElectronico = false;
					do {

						System.out.println(
								"Ingrese el tipo de Electronico(Heladera/Lavarropa/Cocina/TermotanqueElectrico/TermotanqueAGas/Lavavajillas/Ventiladores)");
						tipoElectronico = scan.next();
						switch (tipoElectronico.toLowerCase()) {
						case "heladera":
							if (contadorHeladera < 35) {
								banderaAgregarElectronico = true;
								tipoElectronico = "Heladera";
								contadorHeladera++;

								do {
									System.out.println("Ingrese el modelo de Heladera");
									modeloElectronico = scan.next();
									if (!modeloElectronico.isEmpty()) {
									for (int i = 0; i < TablaElectrodomesticos.electrodomesticos.size(); i++) {
										if (TablaElectrodomesticos.electrodomesticos.get(i).getTipo()
												.equals("Heladera")) {
											if (TablaElectrodomesticos.electrodomesticos.get(i).getModelo()
													.equals(modeloElectronico)) {
												banderaAgregarElectronico = false;
											}
										}
									}
									if (banderaAgregarElectronico == false) {
										System.out.println("El modelo de Heladera ya existe");
									} else {
										System.out.println("Modelo disponible");
									}
									
								}
								else{
									banderaAgregarElectronico =false;
									System.out.println("El codigo no puede ser vacio");
								}
									
								} while (banderaAgregarElectronico == false);
							} else {
								System.out.println("No hay modelos de Heladera disponibles");
							}
							break;

						case "lavarropa":
							if (contadorLavarropa < 22) {
								banderaAgregarElectronico = true;
								tipoElectronico = "Lavarropa";
								contadorLavarropa++;

								do {
									System.out.println("Ingrese el modelo de Lavarropa");
									modeloElectronico = scan.next();
									if (!modeloElectronico.isEmpty()) {
									for (int i = 0; i < TablaElectrodomesticos.electrodomesticos.size(); i++) {
										if (TablaElectrodomesticos.electrodomesticos.get(i).getTipo()
												.equals("Lavarropa")) {
											if (TablaElectrodomesticos.electrodomesticos.get(i).getModelo()
													.equals(modeloElectronico)) {
												banderaAgregarElectronico = false;
											}
										}
									}
									if (banderaAgregarElectronico == false) {
										System.out.println("El modelo de Lavarropa ya existe");
									} else {
										System.out.println("Modelo disponible");
									}
								}
									else{
										banderaAgregarElectronico =false;
										System.out.println("El codigo no puede ser vacio");
									}
								} while (banderaAgregarElectronico == false);
							} else {
								System.out.println("No hay modelos de Lavarropa disponibles");
							}
							break;

						case "cocina":
							if (contadorCocina < 9) {
								banderaAgregarElectronico = true;
								tipoElectronico = "Cocina";
								contadorCocina++;

								do {
									System.out.println("Ingrese el modelo de Cocina");
									modeloElectronico = scan.next();
									if (!modeloElectronico.isEmpty()) {
									for (int i = 0; i < TablaElectrodomesticos.electrodomesticos.size(); i++) {
										if (TablaElectrodomesticos.electrodomesticos.get(i).getTipo()
												.equals("Cocina")) {
											if (TablaElectrodomesticos.electrodomesticos.get(i).getModelo()
													.equals(modeloElectronico)) {
												banderaAgregarElectronico = false;
											}
										}
									}
									if (banderaAgregarElectronico == false) {
										System.out.println("El modelo de Cocina ya existe");
									} else {
										System.out.println("Modelo disponible");
									}
								}
									else{
										banderaAgregarElectronico =false;
										System.out.println("El codigo no puede ser vacio");
									}
								} while (banderaAgregarElectronico == false);
							} else {
								System.out.println("No hay modelos de Cocina disponibles");
							}
							break;

						case "termotanqueelectrico":
							if (contadorTermotanqueElectrico < 4) {
								banderaAgregarElectronico = true;
								tipoElectronico = "TermotanqueElectrico";
								contadorTermotanqueElectrico++;

								do {
									System.out.println("Ingrese el modelo del Termotanque Electrico");
									modeloElectronico = scan.next();
									if (!modeloElectronico.isEmpty()) {
									for (int i = 0; i < TablaElectrodomesticos.electrodomesticos.size(); i++) {
										if (TablaElectrodomesticos.electrodomesticos.get(i).getTipo()
												.equals("TermotanqueElectrico")) {
											if (TablaElectrodomesticos.electrodomesticos.get(i).getModelo()
													.equals(modeloElectronico)) {
												banderaAgregarElectronico = false;
											}
										}
									}
									if (banderaAgregarElectronico == false) {
										System.out.println("El modelo de Termotanque Electrico ya existe");
									} else {
										System.out.println("Modelo disponible");
									}
								}
									else{
										banderaAgregarElectronico =false;
										System.out.println("El codigo no puede ser vacio");
									}
								} while (banderaAgregarElectronico == false);
							} else {
								System.out.println("No hay modelos de Termotanque Electrico disponibles");
							}
							break;

						case "termotanqueagas":
							if (contadorTermotanqueAGas < 9) {
								banderaAgregarElectronico = true;
								tipoElectronico = "TermotanqueAGas";
								contadorTermotanqueAGas++;

								do {
									System.out.println("Ingrese el modelo de Termotanque A Gas");
									modeloElectronico = scan.next();
									if (!modeloElectronico.isEmpty()) {
										for (int i = 0; i < TablaElectrodomesticos.electrodomesticos.size(); i++) {
											if (TablaElectrodomesticos.electrodomesticos.get(i).getTipo()
													.equals("TermotanqueAGas")) {
												if (TablaElectrodomesticos.electrodomesticos.get(i).getModelo()
														.equals(modeloElectronico)) {
													banderaAgregarElectronico = false;
												}
											}
										}
										if (banderaAgregarElectronico == false) {
											System.out.println("El modelo de Termotanque A Gas ya existe");
										} else {
											System.out.println("Modelo disponible");
										}
									
									}else{
										banderaAgregarElectronico =false;
										System.out.println("El codigo no puede ser vacio");
									}
								} while (banderaAgregarElectronico == false);
							} else {
								System.out.println("No hay modelos de Termotanque A Gas disponibles");
							}
							break;

						case "lavavajillas":
							if (contadorLavavajillas < 1) {
								banderaAgregarElectronico = true;
								tipoElectronico = "Lavavajillas";
								contadorLavavajillas++;

								do {
									System.out.println("Ingrese el modelo de Lavavajillas");
									modeloElectronico = scan.next();
									
									if (!modeloElectronico.isEmpty()) {
										
											for (int i = 0; i < TablaElectrodomesticos.electrodomesticos.size(); i++) {
												if (TablaElectrodomesticos.electrodomesticos.get(i).getTipo()
														.equals("Lavavajillas")) {
													if (TablaElectrodomesticos.electrodomesticos.get(i).getModelo()
															.equals(modeloElectronico)) {
														banderaAgregarElectronico = false;
													}
												}
											}
											if (banderaAgregarElectronico == false) {
												System.out.println("El modelo de Lavavajillas ya existe");
											} else {
												System.out.println("Modelo disponible");
											}
									} else
									{
										banderaAgregarElectronico =false;
										System.out.println("El codigo no puede ser vacio");
									}
								} while (banderaAgregarElectronico == false);
							} else {
								System.out.println("No hay modelos de Lavavajillas disponibles");
							}
							break;

						case "ventiladores":
							if (contadorVentiladores < 144) {
								banderaAgregarElectronico = true;
								tipoElectronico = "Ventilador";
								contadorVentiladores++;

								do {
									System.out.println("Ingrese el modelo de Ventilador");
									modeloElectronico = scan.next();
									if (!modeloElectronico.isEmpty()) {
									for (int i = 0; i < TablaElectrodomesticos.electrodomesticos.size(); i++) {
										if (TablaElectrodomesticos.electrodomesticos.get(i).getTipo()
												.equals("Ventilador")) {
											if (TablaElectrodomesticos.electrodomesticos.get(i).getModelo()
													.equals(modeloElectronico)) {
												banderaAgregarElectronico = false;
											}
										}
									}
									if (banderaAgregarElectronico == false) {
										System.out.println("El modelo de Ventilador ya existe");
									} else {
										System.out.println("Modelo disponible");
									}
									
								}
								else{
									banderaAgregarElectronico =false;
									System.out.println("El codigo no puede ser vacio");
								}
									
								} while (banderaAgregarElectronico == false);
							} else {
								System.out.println("No hay modelos de Ventiladores disponibles");
							}
							break;

						default:
							System.out.println("Opcion Incorrecta");
							break;
						}

					} while (banderaAgregarElectronico == false);
					
					do {
					System.out.println("Ingrese la marca del nuevo Electronico");
					scan.next();
					marcaElectronico = scan.nextLine();
					if (!marcaElectronico.isEmpty())
					{
						bandera=false;
						System.out.println("La marca no debe estar vacia");
					}
					else
					{
						bandera=true;
					}
					}while (bandera==false);
					do {
						do {
							bandera = true;
							System.out.println("Ingrese el codigo del nuevo Electronico");
							try {
								codigoElectrodomestico = scan.nextInt();
							} catch (InputMismatchException ime) {
								bandera = false;
								System.out.println("Formato Incorrecto");
								scan.next();
							}
						} while (bandera == false);

						bandera = true;
						for (int i = 0; i < TablaElectrodomesticos.electrodomesticos.size(); i++) {
							if (codigoElectrodomestico == TablaElectrodomesticos.electrodomesticos.get(i).getCodigo()) {
								bandera = false;
								System.out.println("Codigo no disponible");
							}
						}
					} while (bandera == false);

					do {
						bandera = true;
						System.out.println("Ingrese el stock del nuevo Electronico");
						try {
							cantidadStockElectrico = scan.nextInt();
						} catch (InputMismatchException ime) {
							System.out.println("Formato Incorrecto");
							bandera = false;
							scan.next();
						}
					} while (bandera == false);

					do {
						bandera = true;
						System.out.println("Ingrese el precio del Electronico");
						try {
							precioElectronico = scan.nextDouble();
						} catch (InputMismatchException ime) {
							System.out.println("Formato Incorrecto");
							bandera = false;
							scan.next();
						}
					} while (bandera == false);
					Electrodomestico unElectrodomestico = new Electrodomestico(codigoElectrodomestico,
							cantidadStockElectrico, precioElectronico, marcaElectronico, modeloElectronico,
							tipoElectronico);
					unEmpleado.crearElectrodomestico(unElectrodomestico);
				} else {
					System.out.println("Opcion Incorrecta");
				}

				break;
			case 2:
				int lineaPlan = 0, auxCodigo = 0, auxPosicionCliente = 0;
				Electrodomestico unElectrodomestico = new Electrodomestico();
				double precioTotal = 0, montoCuota;
				LocalDate fechaVencimientoCuota;
				Plan unPlan = new Plan();
				String auxOpcion, tipoTarjeta;
				long dniClientePlan = 0;
				ArrayList<Electrodomestico> listaCompra=new ArrayList<Electrodomestico>();
				ArrayList<Cuota> cuotasAux=new ArrayList<Cuota> ();
				boolean tablasConValores=true;
				if (TablaClientes.clientes.isEmpty()) {
					System.out.println("No hay clientes en el sistema");
					tablasConValores=false;
				}
				if (TablaElectrodomesticos.electrodomesticos.isEmpty()) {
					System.out.println("No hay productos en el sistema");
					tablasConValores=false;
				}
				if (tablasConValores==true) {
				do {
					do {
						bandera = true;
						System.out.println("Ingrese el Dni del cliente al que se le agregara el Nuevo Plan");
						try {
							dniClientePlan = scan.nextLong();
						} catch (InputMismatchException ime) {
							bandera = false;
							System.out.println("Formato Incorrecto");
							scan.next();
						}
					} while (bandera == false);

					bandera = false;
					for (int i = 0; i < TablaClientes.clientes.size() && bandera == false; i++) {
						if (TablaClientes.clientes.get(i).getDni() == dniClientePlan) {
							bandera = true;
							auxPosicionCliente = i;
							if (TablaClientes.clientes.get(i).getTipoCliente().equals("Particular")) {
								lineaPlan = 1;
							} else {
								lineaPlan = 2;
							}
						}
					}
					if (bandera == false) {
						System.out.println("El cliente no se encontro");
					}

				} while (bandera == false);

				if (lineaPlan == 1) {
					boolean banderaPlan = false;
					do {
						do {
							do {
								bandera = true;
								System.out.println("Ingrese el codigo del Electrodomestico a Agregar");
								try {
									auxCodigo = scan.nextInt();
								} catch (InputMismatchException ime) {
									bandera = false;
									System.out.println("Formato Incorrecto");
									scan.next();
								}
							} while (bandera == false);

							bandera = false;
							for (int i = 0; i < TablaElectrodomesticos.electrodomesticos.size(); i++) {
								if (auxCodigo == TablaElectrodomesticos.electrodomesticos.get(i).getCodigo()) {
									if (TablaElectrodomesticos.electrodomesticos.get(i).getStock() > 0) {

										unElectrodomestico = TablaElectrodomesticos.electrodomesticos.get(i);
										precioTotal = precioTotal + unElectrodomestico.getPrecio();

										if (precioTotal < 200000) {
											bandera = true;
											TablaElectrodomesticos.electrodomesticos.get(i).setStock(
													TablaElectrodomesticos.electrodomesticos.get(i).getStock() - 1);

											listaCompra.add(unElectrodomestico);
											//unPlan.getArticulos().add(unElectrodomestico);
											System.out.println("Electrodomestico Agregado");
										} else if (precioTotal > 200000) {
											banderaPlan = true;
											System.out.println(
													"Se paso de la cantidad maxima de precio, saliendo de agregar Electrodomesticos y reduciendo el Precio al anterior");
											precioTotal = precioTotal - unElectrodomestico.getPrecio();
										}
									}
								}
								
							}if (bandera == false) {
									System.out.println("No se encontro el Codigo o el Producto no tiene Stock");
								}

						} while (bandera == false);
						if (banderaPlan == false) {
							System.out.println("Desea Agregar mas Electrodomesticos? (si/no)");
							auxOpcion = scan.next();
							if (auxOpcion.equalsIgnoreCase("si")) {
								bandera = false;
							} else if (auxOpcion.equalsIgnoreCase("no")) {
								banderaPlan = true;
							} else {
								System.out.println("Opcion Incorrecta");
							}
						}
					} while (banderaPlan == false);
					unPlan.setArticulos(listaCompra);
					unPlan.setPrecioTotal(precioTotal);
					unPlan.setMontoPorPagar(precioTotal);

					montoCuota = precioTotal / 24;
					fechaVencimientoCuota = LocalDate.now();
					for (int i = 0; i < 24; i++) {

						fechaVencimientoCuota = fechaVencimientoCuota.plusMonths(1);
						Cuota unaCuota = new Cuota(montoCuota, fechaVencimientoCuota, false, null);
						cuotasAux.add(unaCuota);
						TablaCuotas.cuotas.add(unaCuota);
					}
					unPlan.setCuotas(cuotasAux);
					do {
						bandera = false;
						System.out.println("Ingrese el tipo Tarjeta Credito(Macro/FirstWorldBank/HarvestAndTrustee)");
						tipoTarjeta = scan.next();
						switch (tipoTarjeta.toLowerCase()) {
						case "macro":
							bandera = true;
							tipoTarjeta = "Macro";
							break;
						case "firstworldbank":
							bandera = true;
							tipoTarjeta = "First World Bank";
							break;
						case "harvestandtrustee":
							bandera = true;
							tipoTarjeta = "Harvest and Trustee";
							break;
						default:
							System.out.println("Opcion Invalida");
							break;
						}
					} while (bandera == false);
					unPlan.setTipoTarjeta(tipoTarjeta);
					unEmpleado.agregarPlanCliente(auxPosicionCliente, unPlan);
				}

				if (lineaPlan == 2) {
					boolean banderaPlan = false;
					do {
						do {
							do {
								bandera = false;
								try {
									bandera = true;
									System.out.println("Ingrese el codigo del Electrodomestico a Agregar");
									auxCodigo = scan.nextInt();
								} catch (InputMismatchException ime) {
									bandera = false;
									System.out.println("Formato Incorrecto");
									scan.next();
								}
							} while (bandera == false);

							bandera = false;
							for (int i = 0; i < TablaElectrodomesticos.electrodomesticos.size(); i++) {
								if (auxCodigo == TablaElectrodomesticos.electrodomesticos.get(i).getCodigo()) {
									if (TablaElectrodomesticos.electrodomesticos.get(i).getStock() > 0) {

										unElectrodomestico = TablaElectrodomesticos.electrodomesticos.get(i);
										precioTotal = precioTotal + unElectrodomestico.getPrecio();

										if (precioTotal < 500000) {
											bandera = true;
											TablaElectrodomesticos.electrodomesticos.get(i).setStock(
													TablaElectrodomesticos.electrodomesticos.get(i).getStock() - 1);
											listaCompra.add(unElectrodomestico);
											System.out.println("Electrodomestico Agregado");
										} else if (precioTotal > 500000) {
											banderaPlan = true;
											System.out.println(
													"Se paso de la cantidad maxima de precio, saliendo de agregar Electrodomesticos y reduciendo el Precio al anterior");
											precioTotal = precioTotal - unElectrodomestico.getPrecio();
										}
									}
								}
								
							}if (bandera == false) {
									System.out.println("No se encontro el Codigo o el Producto no tiene Stock");
								}

						} while (bandera == false);
						if (banderaPlan == false) {
							System.out.println("Desea Agregar mas Electrodomesticos? (si/no)");
							auxOpcion = scan.next();
							if (auxOpcion.equalsIgnoreCase("si")) {
								bandera = false;
							} else if (auxOpcion.equalsIgnoreCase("no")) {
								banderaPlan = true;
							} else {
								System.out.println("Opcion Incorrecta");
							}
						}
					} while (banderaPlan == false);
					precioTotal = precioTotal * 1.2;
					unPlan.setArticulos(listaCompra);
					unPlan.setPrecioTotal(precioTotal);
					unPlan.setMontoPorPagar(precioTotal);

					montoCuota = precioTotal / 12;
					fechaVencimientoCuota = LocalDate.now();
					for (int i = 0; i < 12; i++) {

						fechaVencimientoCuota = fechaVencimientoCuota.plusMonths(1);
						Cuota unaCuota = new Cuota(montoCuota, fechaVencimientoCuota, false, null);
						cuotasAux.add(unaCuota);
						TablaCuotas.cuotas.add(unaCuota);
					}
					unPlan.setCuotas(cuotasAux);
					do {
						bandera = false;
						System.out.println("Ingrese el tipo Tarjeta Credito(Macro/FirstWorldBank/HarvestAndTrustee)");
						tipoTarjeta = scan.next();
						switch (tipoTarjeta.toLowerCase()) {
						case "macro":
							bandera = true;
							tipoTarjeta = "Macro";
							break;
						case "firstworldbank":
							bandera = true;
							tipoTarjeta = "First World Bank";
							break;
						case "harvestandtrustee":
							bandera = true;
							tipoTarjeta = "Harvest and Trustee";
							break;
						default:
							System.out.println("Opcion Invalida");
							break;
						}
					} while (bandera == false);
					unPlan.setTipoTarjeta(tipoTarjeta);
					unEmpleado.agregarPlanCliente(auxPosicionCliente, unPlan);
				}
			}
				
				break;
			case 3:
				Cliente unClienteCuotas = new Cliente();
				long dniClienteCuotas = 0;
				if (TablaClientes.clientes.isEmpty()) {
					System.out.println("No hay clientes agregados");
				} else {
						do {
							bandera = true;
							System.out.println("Ingrese el dni del Cliente");
							try {
								dniClienteCuotas = scan.nextLong();
							} catch (InputMismatchException ime) {
								System.out.println("Formato Incorrecto");
								bandera = false;
								scan.next();
							}
						} while (bandera == false);

						unClienteCuotas = buscarCliente(dniClienteCuotas);
						if (unClienteCuotas != null) {
							bandera = true;
							unEmpleado.cuotasImpagas(unClienteCuotas);
						} else {
							bandera = false;
						}
				}
				break;
			case 4:
				Cliente unClientePagoCuotas = new Cliente();
				long dniClientePagoCuotas = 0;
				if (TablaClientes.clientes.isEmpty()) {
					System.out.println("No hay clientes en el sistema");
				}
				else {
					do {
						bandera = true;
						System.out.println("Ingrese el dni del Cliente");
						try {
							dniClientePagoCuotas = scan.nextLong();
						} catch (InputMismatchException ime) {
							System.out.println("Formato Incorrecto");
							bandera = false;
							scan.next();
						}
					} while (bandera == false);
					unClientePagoCuotas = buscarCliente(dniClientePagoCuotas);
					if (unClientePagoCuotas != null) {
						bandera = true;
						unEmpleado.registrarPago(unClientePagoCuotas);
					}
					else
					{
						bandera=false;
					}
				}
				break;
			case 5:
				unEmpleado.dineroPorCobrar();
				break;
			case 6:
				if (TablaClientes.clientes.isEmpty()) {
					System.out.println("No hay clientes en el sistema");
				}
				else{
				unEmpleado.clienteMenosDeudor();
				}
				break;
			case 7:
				long dniCliente = 0;
				String nombreCliente = null, apellidoCliente = null, tipoCliente = null;
				do {
					do {
						bandera = true;
						System.out.println("Ingrese el DNI del Cliente");
						try {
							dniCliente = scan.nextLong();
						} catch (InputMismatchException ime) {
							bandera = false;
							System.out.println("Formato Incorrecto");
							scan.next();
						}
					} while (bandera == false);

					bandera = false;
					for (int i = 0; i < TablaClientes.clientes.size() && bandera == false; i++) {
						if (TablaClientes.clientes.get(i).getDni() == dniCliente) {
							bandera = true;
						}
					}
					if (bandera == true) {
						System.out.println("El dni no esta disponible");
						bandera = false;
					} else {
						System.out.println("Dni disponible");
						bandera = true;
					}
				} while (bandera == false);

				System.out.println("Ingrese el nombre del Cliente");
				nombreCliente = scan.next();
				System.out.println("Ingrese el apellido del Cliente");
				apellidoCliente = scan.next();
				do {
					bandera = false;
					System.out.println("Ingrese el tipo Cliente(Particular/Empresa)");
					tipoCliente = scan.next();
					if (tipoCliente.equalsIgnoreCase("Particular")) {
						tipoCliente = "Particular";
						bandera = true;
					} else if (tipoCliente.equalsIgnoreCase("Empresa")) {
						tipoCliente = "Empresa";
						bandera = true;
					} else {
						System.out.println("No valido");
						bandera = false;
					}
				} while (bandera == false);

				Cliente unCliente = new Cliente(dniCliente, nombreCliente, apellidoCliente, tipoCliente, null);
				unEmpleado.crearCliente(unCliente);

				break;
			case 8:
				System.out.println("Saliendo del Programa");
				break;
			default:
				System.out.println("Opcion Incorrecta");
				break;
			}
		} while (opcion != 8);

		scan.close();
	}

	private static Cliente buscarCliente(long dni) {
		boolean band = false;
		for (int i = 0; i < TablaClientes.clientes.size() && band == false; i++) {
			if (TablaClientes.clientes.get(i).getDni() == dni) {
				band = true;
				Cliente unCliente = TablaClientes.clientes.get(i);
				return unCliente;
			}
		}
		if (band == false) {
			System.out.println("No existe un cliente con dicho dni");
		}
		return null;
	}

}
