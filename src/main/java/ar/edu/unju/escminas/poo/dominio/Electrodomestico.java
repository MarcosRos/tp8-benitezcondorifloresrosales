package ar.edu.unju.escminas.poo.dominio;

public class Electrodomestico {

	private int codigo;
	private int stock;
	private double precio;
	private String marca;
	private String modelo;
	private String tipo;

	public Electrodomestico() {
		// TODO Auto-generated constructor stub
	}

	public Electrodomestico(int codigo, int stock, double precio, String marca, String modelo, String tipo) {
		super();
		this.codigo = codigo;
		this.stock = stock;
		this.precio = precio;
		this.marca = marca;
		this.modelo = modelo;
		this.tipo = tipo;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Electrodomestico [codigo=" + codigo + ", stock=" + stock + ", precio=" + precio + ", marca=" + marca
				+ ", modelo=" + modelo + ", tipo=" + tipo + "]";
	}
	
	

}

