package ar.edu.unju.escminas.poo.dominio;

import java.util.ArrayList;

public class Plan {
    private double precioTotal;
    private int linea;
    private String tipoTarjeta;
    private double montoPorPagar;
    private ArrayList<Cuota> cuotas;
    private ArrayList<Electrodomestico> articulos;
    
    public Plan() {
        // TODO Auto-generated constructor stub
    }

    public Plan(double precioTotal, int linea, String tipoTarjeta, double montoPorPagar, ArrayList<Cuota> cuotas,
            ArrayList<Electrodomestico> articulos) {
        super();
        this.precioTotal = precioTotal;
        this.linea = linea;
        this.tipoTarjeta = tipoTarjeta;
        this.montoPorPagar = montoPorPagar;
        this.cuotas = cuotas;
        this.articulos = articulos;
    }

    public double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public String getTipoTarjeta() {
        return tipoTarjeta;
    }

    public void setTipoTarjeta(String tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public double getMontoPorPagar() {
        return montoPorPagar;
    }

    public void setMontoPorPagar(double montoPorPagar) {
        this.montoPorPagar = montoPorPagar;
    }

    public ArrayList<Cuota> getCuotas() {
        return cuotas;
    }

    public void setCuotas(ArrayList<Cuota> cuotas) {
        this.cuotas = cuotas;
    }

    public ArrayList<Electrodomestico> getArticulos() {
        return articulos;
    }

    public void setArticulos(ArrayList<Electrodomestico> articulos) {
        this.articulos = articulos;
    }

    @Override
    public String toString() {
        return "Plan [precioTotal=" + precioTotal + ", linea=" + linea + ", tipoTarjeta=" + tipoTarjeta
                + ", montoPorPagar=" + montoPorPagar + ", articulos=" + articulos + "]";
    }
    
}