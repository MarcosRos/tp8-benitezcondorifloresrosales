package ar.edu.unju.escminas.poo.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ EmpleadoTest.class })
public class TestSuiteTp8 {

}
