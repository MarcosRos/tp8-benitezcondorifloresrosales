package ar.edu.unju.escminas.poo.test;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;

import ar.edu.unju.escminas.poo.dominio.Cliente;
import ar.edu.unju.escminas.poo.dominio.Cuota;
import ar.edu.unju.escminas.poo.dominio.Empleado;
import ar.edu.unju.escminas.poo.dominio.Plan;
import ar.edu.unju.escminas.poo.tablas.TablaClientes;
import junit.framework.TestCase;

public class EmpleadoTest extends TestCase {

    @Override
    protected void setUp() throws Exception {
        // TODO Auto-generated method stub
        System.out.println("InicioTest");
        //super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        // TODO Auto-generated method stub
        System.out.println("FinTest");
        //super.tearDown();
    }

    @Test
    public void testVerificarCuotasNull() {
        Empleado unEmpleadom= new Empleado("Juna","asd123");
        ArrayList<Cuota> unasCuotasm= new ArrayList<Cuota>();
        Cuota unaCuota=new Cuota(192,LocalDate.now(),false,null);
        
        for(int i=0;i<12;i++) {
            unasCuotasm.add(unaCuota);
        }
        Plan unPlam= new Plan(192*12,2,"Macro",192*12,unasCuotasm,null);
        Cliente unClientem = new Cliente(15231,"Lucas","Campos","Empresa",unPlam);
        unEmpleadom.crearCliente(unClientem);
        
        assertNotNull(TablaClientes.clientes.get(0).getPlanCliente().getCuotas());
    }
    
    
    @Test
    public void testVerificarCantidadCuotas12() {
        Empleado unEmpleadom= new Empleado("Juna","asd123");
        ArrayList<Cuota> unasCuotasm= new ArrayList<Cuota>();
        Cuota unaCuota=new Cuota(192,LocalDate.now(),false,null);
        
        for(int i=0;i<12;i++) {
            unasCuotasm.add(unaCuota);
        }
        Plan unPlam= new Plan(192*12,2,"Macro",192*12,unasCuotasm,null);
        Cliente unClientem = new Cliente(15231,"Lucas","Campos","Empresa",unPlam);
        unEmpleadom.crearCliente(unClientem);
        
        assertEquals(12, TablaClientes.clientes.get(0).getPlanCliente().getCuotas().size());
    }
    
    @Test
    public void testVerificarCantidadCuotas24() {
        Empleado unEmpleadom= new Empleado("Anuj","321dsa");
        ArrayList<Cuota> unasCuotasm= new ArrayList<Cuota>();
        Cuota unaCuota=new Cuota(111,LocalDate.now(),false,null);
        
        for(int i=0;i<24;i++) {
            unasCuotasm.add(unaCuota);
        }
        Plan unPlam= new Plan(111*24,1,"Macro",111*24,unasCuotasm,null);
        Cliente unClientem = new Cliente(15231,"Sacul","Sopmac","Particular",unPlam);
        unEmpleadom.crearCliente(unClientem);
        
        assertEquals(12, TablaClientes.clientes.get(0).getPlanCliente().getCuotas().size());
    }
    
    @Test
    public void testVerificarCreditoL1() {
        Empleado unEmpleadom= new Empleado("Anuj","321dsa");
        ArrayList<Cuota> unasCuotasm= new ArrayList<Cuota>();
        Cuota unaCuota=new Cuota(111,LocalDate.now(),false,null);
        
        for(int i=0;i<24;i++) {
            unasCuotasm.add(unaCuota);
        }
        Plan unPlam= new Plan(111*24,1,"Macro",111*24,unasCuotasm,null);
        Cliente unClientem = new Cliente(15231,"Sacul","Sopmac","Particular",unPlam);
        unEmpleadom.crearCliente(unClientem);
        
        boolean bandera=false;
        if(TablaClientes.clientes.get(0).getPlanCliente().getPrecioTotal()<200000) {
            bandera=true;
        }
        assertEquals(true,bandera);
    }
    
    @Test
    public void testVerificarCreditoL2() {
        Empleado unEmpleadom= new Empleado("Juna","asd123");
        ArrayList<Cuota> unasCuotasm= new ArrayList<Cuota>();
        Cuota unaCuota=new Cuota(192,LocalDate.now(),false,null);
        
        for(int i=0;i<12;i++) {
            unasCuotasm.add(unaCuota);
        }
        Plan unPlam= new Plan(192*12,2,"Macro",192*12,unasCuotasm,null);
        Cliente unClientem = new Cliente(15231,"Lucas","Campos","Empresa",unPlam);
        unEmpleadom.crearCliente(unClientem);
        
        boolean bandera=false;
        if(TablaClientes.clientes.get(0).getPlanCliente().getPrecioTotal()<500000) {
            bandera=true;
        }
        assertEquals(true,bandera);
    }
}